﻿using System.ServiceModel;

namespace CalculatorServiceHttps.Contracts
{
    [ServiceContract]
    public interface ICalculatorService
    {
        [OperationContract]
        int Sum(int a, int b);

        [OperationContract]
        int Sub(int a, int b);

        [OperationContract]
        int Mul(int a, int b);

        [OperationContract]
        double Div(int a, int b);
    }
}
