﻿using System;
using System.ServiceModel;
using CalculatorServiceHttps.Contracts;

namespace CalculatorServiceHttps
{
    public class CalculatorService : ICalculatorService
    {
        public double Div(int a, int b)
        {
            if (b == 0)
            {
                throw new FaultException("Попытка деления на 0");
            }

            return a / (double)b;
        }

        public int Mul(int a, int b)
        {
            try
            {
                return checked(a * b);
            }
            catch (OverflowException)
            {
                throw new FaultException("Overflow");
            }
        }

        public int Sub(int a, int b)
        {
            try
            {
                return checked(a - b);
            }
            catch (OverflowException)
            {
                throw new FaultException("Overflow");
            }
        }

        public int Sum(int a, int b)
        {
            try
            {
                return checked(a + b);
            }
            catch (OverflowException)
            {
                throw new FaultException("Overflow");
            }
        }
    }
}
